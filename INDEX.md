# SAMCFG

Set of sample files for creating C:CONFIG.SYS and C:AUTOEXEC.BAT from previous versions of FreeDOS.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## SAMCFG.LSM

<table>
<tr><td>title</td><td>SAMCFG</td></tr>
<tr><td>version</td><td>20110719a</td></tr>
<tr><td>entered&nbsp;date</td><td>2011-07-19</td></tr>
<tr><td>description</td><td>Sample configuration files</td></tr>
<tr><td>summary</td><td>Set of sample files for creating C:CONFIG.SYS and C:AUTOEXEC.BAT from previous versions of FreeDOS.</td></tr>
<tr><td>keywords</td><td>freedos, sample, configuration, config.sys, autoexec.bat, batch, script</td></tr>
<tr><td>author</td><td>Bernd Blaauw &lt;bblaauw@home.nl&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Bernd Blaauw &lt;bblaauw@home.nl&gt;</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Public domain](LICENSE)</td></tr>
</table>
